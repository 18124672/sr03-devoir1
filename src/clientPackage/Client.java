package clientPackage;
import java.io.IOException;
import java.net.Socket;

public class Client {
    public static void main(String[] args){
        Socket client = null;
        try {
            client = new Socket("localhost",10800);
            System.out.println("client start");
            ClientReceptor clientreceptor = new ClientReceptor(client);
            ClientSender clientsender = new ClientSender(client);
            clientsender.start();
            clientreceptor.start();
            clientreceptor.join();
            clientsender.join();
            client.close();
        } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
        }
    }
}

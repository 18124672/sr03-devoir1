package clientPackage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ClientSender extends Thread{
    private Socket socket;
    public ClientSender(Socket socket){
        this.socket = socket;
    }
    @Override
    public void run(){
        DataOutputStream out;
        try {
            Scanner scanner = new Scanner(System.in);
            out = new DataOutputStream(socket.getOutputStream());
            while(true){
                String msg = scanner.nextLine();
                out.writeUTF(msg);
                if(msg.equals("exit")){
                    break;
                }
            }
        } catch (IOException e) {
           System.out.println("Bye!");
        }
    }
}

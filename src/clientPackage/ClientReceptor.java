package clientPackage;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientReceptor extends Thread{
    private final Socket socket;
    public ClientReceptor(Socket socket){
        this.socket = socket;
    }
    @Override
    public void run(){
        DataInputStream in;
        try {
            in = new DataInputStream(socket.getInputStream());
            String msg;
            while(true){
                msg = in.readUTF();
                if (msg.equals("exit")){
                    System.out.println("Bye!");
                    break;
                }
                System.out.println(msg);
            }
        } catch (IOException e) {
            try {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
                System.out.println("Server is closed, press enter to quit");
            } catch (IOException ex) {
                throw new RuntimeException(e);
            }
        }
    }
}

package serverPackage;

import java.io.*;
import java.net.Socket;

public class MySocket {
    private String name;
    private Socket socket;

    public MySocket(String name, Socket socket) {
        this.name = name;
        this.socket = socket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public DataInputStream getDataInputStream(){
        try {
            DataInputStream ins = new DataInputStream(socket.getInputStream());
            return ins;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public DataOutputStream getDataOutputStream(){
        try {
            DataOutputStream outs = new DataOutputStream(socket.getOutputStream());
            return outs;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

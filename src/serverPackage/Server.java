package serverPackage;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedDeque;

public class Server{
    private static ConcurrentLinkedDeque<MySocket> clients = new ConcurrentLinkedDeque<>();
    public static ConcurrentLinkedDeque<MySocket> getClients() { return clients;}
    public static void main(String []args){
        try {
            ServerSocket serverSocket = new ServerSocket(10800);
            clients = getClients();
            System.out.println("server start");
            while (true) {
                Socket newsocket = serverSocket.accept();
                MySocket newmysocket = new MySocket(null,newsocket);
                clients.add(newmysocket);
                ServerReceptor serverreceptor = new ServerReceptor(newmysocket);
                serverreceptor.start();//start a thread
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

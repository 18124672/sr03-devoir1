package serverPackage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import static serverPackage.Server.getClients;


public class ServerReceptor extends Thread{
    private final MySocket mysocket;
    public ServerReceptor(MySocket mysocket){
        this.mysocket = mysocket;
    }
    @Override
    public void run(){
        try {
            DataInputStream ins = mysocket.getDataInputStream();
            DataOutputStream out = mysocket.getDataOutputStream();
            int flag = 0;
            StringBuilder msg;
            String name;
            out.writeUTF("Enter your name");
            while(true){    // initialise name at first time
                name = ins.readUTF();
                for(MySocket client: getClients()){
                    if(name.equals(client.getName())){
                        out.writeUTF("there is another " + name);
                        out.writeUTF("please enter another name");
                        flag = 1; // check for duplicate nicknames
                        break;
                    }
                    flag = 0; // no duplicate nicknames
                }
               if(flag == 0) {
                   break; // name entered is unique
               }
            }
            mysocket.setName(name);

            for(MySocket client: getClients()){
                DataOutputStream outs = client.getDataOutputStream();
                outs.writeUTF(name+ " a rejoint la conservation");
            }
            out.writeUTF("-----------");

            while(true){
                msg = new StringBuilder(ins.readUTF());
                System.out.println("message "+ msg);
                if (msg.toString().equals("exit")){
                    out.writeUTF("exit");
                }
                if (msg.toString().equals("exit")){
                    msg = new StringBuilder(" a exit");
                }
                else{
                    msg.insert(0, " a dit ");
                }
                for(MySocket client: getClients()){
                    DataOutputStream outs = client.getDataOutputStream();
                    outs.writeUTF(name + msg);
                }
            }
        } catch (IOException e) {
            try {
                getClients().remove(mysocket);
                mysocket.getSocket().close();
                System.out.println("close");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
